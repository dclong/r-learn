# Code for comparing the OLS and GLM estimates for AR(1) processes.
# Actually it be proved that under normal assumption, 
# OLS estimate is MLE for AR1X process.
#
gen.data = function(a, b, s2, n){
    s2.y = s2/(1 - a^2)
    s.y = sqrt(s2.y)
    s.error = sqrt(s2)
    y = rep(NA, n+1)
    x = 1:n
    y[1] = rnorm(n = 1, mean = 0, sd = s.y)
    for(i in 2:(n+1)){
        y[i] = a * y[i-1] + b * x[i-1] + rnorm(n=1, mean=0, sd=s.error)
    }
    y = y[-1]
    list(y=y, x=x)
}

parhat.ols = function(d){
    x = d$x
    y = d$y
    n = length(x)
    x = cbind(y[-n], x[-1])
    y = y[-1]
    tx = t(x)
    solve(tx%*%x)%*%tx%*%y -> abhat
    s2hat = sum((y - x%*%abhat)^2)/(n-3)
    c(abhat, s2hat)
}

parhat.glm = function(d){
    x = d$x
    y = d$y
    n = length(x)
    x = x[-1]
    ylag1 = y[-n]
    y = y[-1]
    sub = rep(1, n-1)
    d = cbind(ylag1, x, sub, y)
    colnames(d) = c("ylag1", "x", "sub", "y")
    d = as.data.frame(d)
    gls(y ~ ylag1 + x - 1, data = d, correlation = corAR1(form = ~ 1 | sub)) -> f
    c(f$coefficients, f$sigma^2)
}

sim.ar1x = function(a, b, s2, n ,n.sim){
    library(nlme)
    matrix(0, nrow=n.sim, ncol=3) -> bhat.ols
    bhat.glm = bhat.ols
    cns = c("a", "b", "s2")
    colnames(bhat.ols) = cns
    colnames(bhat.glm) = cns
    for(i in 1:n.sim){
        d = gen.data(a, b, s2, n)
        parhat.ols(d) -> bhat.ols[i, ]
        parhat.glm(d) -> bhat.glm[i, ]
    }
    par = c(a, b, s2)
    rowMeans((t(bhat.ols) - par)^2) -> mse.ols
    rowMeans((t(bhat.glm) - par)^2) -> mse.glm
    list(par=c(a, b, s2), parhat.ols=bhat.ols, parhat.glm=bhat.glm, mse.ols=mse.ols, mse.glm=mse.glm)
}


#' @TODO: make it accept an optional argument 
# to read replacement rules from a file.
format_name = function(name){
    # get rid of leading white spaces
    name = stringr::str_trim(name, side="both")
    # replace special characters with underscores
    name = gsub(" ", "_", name, fixed=TRUE) 
    name = gsub("-", "_", name, fixed=TRUE)
    name = gsub("(", "_", name, fixed=TRUE)
    name = gsub(")", "_", name, fixed=TRUE)
    name = gsub("/", "_", name, fixed=TRUE)
    name = gsub("%", "_", name, fixed=TRUE)
    name = gsub(":", "_", name, fixed=TRUE)
    name = gsub(".", "_", name, fixed=TRUE)
    # replace multiple consecutive underscores with 1 underscore
    name = gsub("_+", "_", name)
    return(name)
}

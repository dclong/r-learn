source('C:/Users/bdu01/Downloads/archives/code/r/ccar/backtesting_ar1x.r')
source('C:/Users/bdu01/Downloads/archives/code/r/ccar/linear_regression.r')
source('C:/Users/bdu01/Downloads/archives/code/r/ccar/plot_bt.r')
source('C:/Users/bdu01/Downloads/archives/code/r/ccar/plot_ccar.r')
source('C:/Users/bdu01/Downloads/archives/code/r/ccar/predict_ar1x.r')

# data
heloc[100, "NCO_PER_L1"] = heloc[99, "NCO_PER"]
heloc.ub = heloc[3:89, ]
heloc.hist = heloc[3:99, ]
heloc.all = heloc[3:112, ]
heloc.ccar = heloc[100:112, ]

# formulas
formula.b = "NCO_PER ~ NCO_PER_L1 + LHPIRT_b + URD_b"
formula.a = "NCO_PER ~ NCO_PER_L1 + LHPIRT_a + URD_a"
formula.s = "NCO_PER ~ NCO_PER_L1 + LHPIRT_s + URD_s"

# fit a simple linear regression using data until 2012 Q1, i.e., a ub model
lm(formula.b, data=heloc.ub) -> lm.ub
summary(lm.ub)
lm.ub

# bmo models
lm(formula.b, data=heloc.hist) -> lm.bmo.b
lm(formula.a, data=heloc.hist) -> lm.bmo.a
lm(formula.s, data=heloc.hist) -> lm.bmo.s





predict_ar1x(heloc.ccar, "NCO_PER_L1", lm.bmo.b) -> p.b
predict_ar1x(heloc.ccar, "NCO_PER_L1", lm.bmo.a) -> p.a
predict_ar1x(heloc.ccar, "NCO_PER_L1", lm.bmo.s) -> p.s



# plots 3 scenarios together

plot_ccar(heloc.all, "NCO_PER", p.b$y, p.a$y, p.s$y,
          ylab="Mortgage NCO (%)",
          main="CCAR 2015 Scenario Tests for Industry HELOC")


lines(1:length(lm.bmo.b$fitted.values), lm.bmo.b$fitted.values, type="p", cex=0.4, col="purple")
# in-sample backtesting, fitting

backtesting_ar1x(formula.b, heloc.hist, "NCO_PER_L1", 9) -> b.9
backtesting_ar1x(formula.b, heloc.hist, "NCO_PER_L1", 18) -> b.18
backtesting_ar1x(formula.b, heloc.hist, "NCO_PER_L1", 22) -> b.22
backtesting_ar1x(formula.b, heloc.hist, "NCO_PER_L1", 27) -> b.27

plot_bt(heloc.hist, "NCO_PER", b.9$y, b.18$y, b.27$y, ylim=c(0, 3.5))
plot_bt(heloc.hist, "NCO_PER", b.9$y, b.22$y, b.27$y, ylim=c(0, 3.5))


predict_ar1x(heloc[91:99, ], "NCO_PER_L1", lm.bmo.b) -> ib.9
predict_ar1x(heloc[82:99, ], "NCO_PER_L1", lm.bmo.a) -> ib.18
predict_ar1x(heloc[73:99, ], "NCO_PER_L1", lm.bmo.s) -> ib.27

plot_bt(heloc.hist, "NCO_PER", ib.9$y, ib.18$y, ib.27$y, ylim=c(0, 3.5))



select_lags_ar1x(heloc[1:81, ], 3) -> lag.18
select_lags_ar1x(heloc[1:72, ], 3) -> lag.27

lm(NCO_PER ~ NCO_PER_L1 + LHPIRT_b + URD_b2, data=heloc[4:81, ]) -> lm.bmo.b2
lm(NCO_PER ~ NCO_PER_L1 + LHPIRT_b0 + URD_b2, data=heloc[4:72, ]) -> lm.bmo.b27
lm(NCO_PER ~ NCO_PER_L1 + LHPIRT_b2 + URD_b2, data=heloc[4:72, ]) -> lm.bmo.b27.22

predict_ar1x(heloc[82:99, ], "NCO_PER_L1", lm.bmo.b2) -> ob.18
predict_ar1x(heloc[73:99, ], "NCO_PER_L1", lm.bmo.b27) -> ob.27
predict_ar1x(heloc[73:99, ], "NCO_PER_L1", lm.bmo.b27.22) -> ob.27.22

plot_bt(heloc[4:99, ], "NCO_PER", ob.18$y, ob.27$y, ylim=c(0, 3.5))
plot_bt(heloc[4:99, ], "NCO_PER", ob.18$y, ob.27.22$y, ylim=c(0, 3.5))



trace_peak(formula.b, heloc.hist, p.b$y) -> c.b
trace_peak(formula.a, heloc.hist, p.a$y) -> c.a
trace_peak(formula.s, heloc.hist, p.s$y) -> c.s

plot(72:80, c.s[, 2], type ="b", col="blue")
lines(72:80, p.s$x[1:9, 2], type="b", lty="dashed", col="red")

plot(72:80, c.s[, 3], type ="b", col="blue")
lines(72:80, p.s$x[1:9, 3], type="b", lty="dashed", col="red")


plot(72:80, c.s[, 4], type ="b", col="blue")
lines(72:80, p.s$x[1:9, 4], type="b", lty="dashed", col="red")


predict.ar1x(heloc[72:112, ], "NCO_PER_L1", lm.bmo.s) -> j

predict.ar1x(heloc[4:112, ], "NCO_PER_L1", lm.bmo.s) -> k


save.image('heloc.rdata')

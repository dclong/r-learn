
tran_hpi = function(hpi, lag){
    l1 = shift.right(x=hpi, padding=NA, k=lag)
    l2 = shift.right(x=hpi, padding=NA, k=lag+1)
    log12 = log(l1/l2)
    ifelse(log12>0, 0, log12)
}

tran_ur = function(ur, lag){
    l1 = shift.right(x=ur, padding=NA, k=lag)
    l2 = shift.right(x=ur, padding=NA, k=lag+1)
    l1 - l2
}

#' Calculate the log likelihood of the AR1X model.
#' This code treat the AR1X as a simple linear regression.
#' I know this is not the right way to calculate the log likelihood for AR1X model, 
#' but everyone do it in this way.
#' 
#' @param lmout a linear model output (typically output of lm).
#' @return the log likelihood of the corresponding model
llh_ar1x = function(lmout){
    residuals = lmout$residuals
    n = length(residuals)
    df = lmout$df.residual 
    s2 = sum(residuals^2) / df
    -0.5 * (df + n * log(s2))
}

select_lags_ar1x = function(data, startIndex){
    stat = NULL
    for(i in 0:3){# lags for UR
        tran_ur(data$UR_b, i) -> URD
        for(j in 0:3){
            tran_hpi(data$HPI_b, j) -> LHPIRT
            ndata = cbind(data[, c("NCO_PER", "NCO_PER_L1")], URD, LHPIRT)
            # fit model
            formula = "NCO_PER ~ NCO_PER_L1 + URD + LHPIRT"
            lm(formula=formula, data=ndata[startIndex:nrow(ndata), ]) -> lmout
            # log likelihood, R square, P values, sign, etc
            llh = llh_ar1x(lmout)
            lmout.summary = summary(lmout)
            r2 = lmout.summary$r.squared
            coef = lmout.summary$coefficients
            est = coef[, "Estimate"]
            p = coef[, "Pr(>|t|)"]
            stat = rbind(stat, c(i, j, llh, r2, est, p))
        }
    }
    colnames(stat) = c("Lag - UR", "Lag - HPI", "LLH", "R-squared", "Est - (Intercep)", "Est - L1", "Est - URD", "Est - LHPIRT", "P - (Intercep)", "P - L1", "P - URD", "P - LHPIRT")
    stat
}

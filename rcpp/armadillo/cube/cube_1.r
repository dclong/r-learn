#' The constructor tested here requires a raw pointer instead of an iterator.
#' It's either the .begin() method of NumbericVector returns a raw pointer, 
#' or Rcpp does a silent transform.
require(inline)
src <- '
using namespace Rcpp;

NumericVector vecArray(myArray);
IntegerVector arrayDims(myDims);

arma::cube cubeArray(vecArray.begin(), arrayDims[0], arrayDims[1], arrayDims[2]);

//change one element in the array/cube
cubeArray(0,0,0) = 518;  

return(wrap(cubeArray));  
'
 
readCube = cxxfunction(signature(myArray="numeric", myDims="integer"),body=src, plugin="RcppArmadillo")

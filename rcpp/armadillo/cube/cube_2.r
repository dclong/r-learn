# similar to 3-d arrays in R (check on this)
require(inline)
src <- '
using namespace Rcpp;

NumericVector vecArray(myArray);
IntegerVector arrayDims(myDims);

arma::cube cubeArray(vecArray.begin(), arrayDims[0], arrayDims[1], arrayDims[2]);

//change one element in the array/cube
cubeArray(0,0,0) = 518;  
cubeArray.slice(1) = cubeArray.slice(2);
return(wrap(-cubeArray));
'

readCube = cxxfunction(signature(myArray="numeric", myDims="integer"),body=src, plugin="RcppArmadillo")

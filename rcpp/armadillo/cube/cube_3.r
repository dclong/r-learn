require(inline)
src <- '
using namespace Rcpp;
NumericVector vecArray(myArray);
IntegerVector arrayDims(myDims);
arma::cube cubeArray(vecArray.begin(), arrayDims[0], arrayDims[1], arrayDims[2]);

arma::mat m = join_cols(cubeArray.slice(1), cubeArray.slice(2));
return(wrap(m));
'

readCube = cxxfunction(signature(myArray="numeric", myDims="integer"),body=src, plugin="RcppArmadillo")
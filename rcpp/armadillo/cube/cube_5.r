require(inline)
src <- '
using namespace Rcpp;
NumericVector vecArray(myArray);
IntegerVector arrayDims(myDims);
arma::cube cubeArray(vecArray.begin(), arrayDims[0], arrayDims[1], arrayDims[2]);
// slices indexes inclusive, which is confusing ...
// this does not work!!!
arma::cube m = join_slices(cubeArray.slice(2), cubeArray.slice(0));
return(wrap(m));
'

readCube = cxxfunction(signature(myArray="numeric", myDims="integer"),body=src, plugin="RcppArmadillo")
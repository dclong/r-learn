require(inline)
src <- '
using namespace Rcpp;
NumericVector vecArray(myArray);
IntegerVector arrayDims(myDims);
arma::cube cubeArray(vecArray.begin(), arrayDims[0], arrayDims[1], arrayDims[2]);
// slices indexes inclusive, which is confusing ...
arma::cube m = cubeArray.slices(2,3);
m.insert_slices(1,cubeArray.slices(0, 1));
return(wrap(m));
'

readCube = cxxfunction(signature(myArray="numeric", myDims="integer"),body=src, plugin="RcppArmadillo")
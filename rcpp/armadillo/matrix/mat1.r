require(inline)
src <- '
using namespace Rcpp;
arma::mat m(as<arma::mat>(r_m));
m = m + 1;
return(wrap(m));
'

mat = cxxfunction(signature(r_m="Numeric"),body=src, plugin="RcppArmadillo")
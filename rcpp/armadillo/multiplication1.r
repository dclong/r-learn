# this shows that the armadillo is OK row vector and matrix multiplication
library(inline)
library(Rcpp)

src = "
using namespace Rcpp;
arma::mat Y(as<arma::mat>(r_Y));
arma::mat X(as<arma::mat>(r_X));
arma::rowvec y = Y.row(0);
y = y * X;
return wrap(y);
"
includes = paste(readText(file='/home/dclong/Dropbox/code/r/package/spt2/dclong.spt/inst/test_statistics.cpp'), "\n\n", 
                 readText(file='/home/dclong/Dropbox/code/r/package/spt2/dclong.spt/inst/spt.cpp'),sep="")

settings=getPlugin("RcppArmadillo")
settings$env$PKG_CXXFLAGS=paste('-std=c++0x ',settings$env$PKG_CXXFLAGS)
fx = cxxfunction(signature(r_Y="numeric", r_X="numeric"),body=src,includes=includes, 
                 plugin="RcppArmadillo",settings=settings)

# subview can be used to initialize matrix, but it doesn't have iterators and so on ...
require(inline)
src <- '
using namespace Rcpp;
arma::mat m(as<arma::mat>(r_m));
arma::rowvec r = m.row(0);
return(wrap(r));
'

mat = cxxfunction(signature(r_m="Numeric"),body=src, plugin="RcppArmadillo")
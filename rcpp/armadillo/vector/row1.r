require(inline)
src <- '
using namespace Rcpp;
arma::mat m(as<arma::mat>(r_m));
arma::rowvec r = m.row(0) * m;
return(wrap(r));
'

mat = cxxfunction(signature(r_m="Numeric"),body=src, plugin="RcppArmadillo")
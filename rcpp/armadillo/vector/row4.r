# this code shows that this Rcpp sugar not only work on 
# a rowvec in Armadillo but also returns the same type. 

require(inline)
src <- '
using namespace Rcpp;
arma::mat m(as<arma::mat>(r_m));
arma::rowvec r = m.row(0) * m;
arma::rowvec rabs = abs(r*-1);
return(wrap(max(rabs)));
'

mat = cxxfunction(signature(r_m="Numeric"),body=src, plugin="RcppArmadillo")
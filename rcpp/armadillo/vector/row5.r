# this code shows that subvec works fine, also the index is inclusive both
require(inline)
src <- '
using namespace Rcpp;
arma::mat m(as<arma::mat>(r_m));
arma::rowvec r = m.row(0) * m;
arma::rowvec rabs = abs(r*-1);
rabs = rabs.subvec(0,1);
return(wrap(rabs));
'

mat = cxxfunction(signature(r_m="Numeric"),body=src, plugin="RcppArmadillo")
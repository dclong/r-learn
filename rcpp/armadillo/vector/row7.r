require(inline)
src <- '
using namespace Rcpp;
arma::mat m(as<arma::mat>(r_m));
arma::rowvec r = m.row(0);
shuffle_left(r.begin(), r.end(), r.size());
return(wrap(r));
'
includes = '
template<typename InputIt> void shuffle_left(InputIt first, InputIt last, int k) {
    RNGScope scope;
    int n = last - first;
    std::cout << "n: " << n << std::endl;
    int n_minus_one = n - 1;
    int upperBound = k;
    //no need to shuffle if only 1 element is left
	if (upperBound>=n) {
		upperBound = n_minus_one;
	}
    for(InputIt it=first; it!=last; ++it){
        std::cout << *it << " ";
    }
    std::cout << std::endl;
	for (int i=0; i<upperBound; ++i) {
		//generate an index between i and n - 1 (inclusive)
		int index = static_cast<int>(Rf_runif(i, n));
        std::cout << "index: " << index << std::endl;
        index = std::min(index, n);
        index = std::max(index, i-1);
        // size_t index = k;
		//swap x[i] and x[index]
        std::swap(*(first + i), *(first + index));
        for(InputIt it=first; it!=last; ++it){
            std::cout << *it << " ";
        }
        std::cout << std::endl;
	}
}
'
mat = cxxfunction(signature(r_m="Numeric"),body=src, plugin="RcppArmadillo", includes=includes)

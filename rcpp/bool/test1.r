#' It seems that the data type in R doesn't matter.
require(inline)
src <- '
bool b(as<bool>(r_b));
std::cout << std::boolalpha << b << std::endl;
'
settings=getPlugin("Rcpp")
settings$env$PKG_CXXFLAGS=paste('-std=c++11', settings$env$PKG_CXXFLAGS)
passbool = cxxfunction(signature(r_b="logical"), body=src, settings=settings)

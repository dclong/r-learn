library(Rcpp)
library(inline)
code = 'std::vector<double> v{1,2,3};
double x = as<double>(rnorm(1, 100));
v.push_back(x);
return wrap(v);'
settings=getPlugin("Rcpp")
settings$env$PKG_CXXFLAGS=paste('-std=c++11 ',settings$env$PKG_CXXFLAGS)
fcpp11 = cxxfunction(body=code,includes="#include <vector>",settings=settings)

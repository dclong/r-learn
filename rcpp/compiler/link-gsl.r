
code = '
int n(as<int>(r_n));
double p(as<double>(r_p));
std::vector<double> x(n);
ralpha_wrapper(x.begin(), x.end(), p);
return wrap(x);
'
includes = readText('beta.cpp')
settings=getPlugin("Rcpp")
settings$env$PKG_LIBS = paste('-lgsl -lgslcblas ', settings$env$PKG_LIBS)
settings$env$PKG_CXXFLAGS=paste('-std=c++0x ',settings$env$PKG_CXXFLAGS)
ralpha = cxxfunction(signature(r_n="integer", r_p="double"), body=code, includes=includes, settings=settings)

library(Rcpp)
library(inline)
############################################################################################
# this does not work because l[0] is sexp which do not support []

code = 'List l(r_list);
return l[0][0] + 1;
'
test_list = cxxfunction(signature(r_list="list"),body=code,plugin="Rcpp")

l = list(a=1:10, b='hahaha')

print(test_list(l))

###########################################################################################
# no nice way to convert list to vector of vectors except using a loop
# the containsElementNamed method accept const char * not std::string

code = 'std::vector<std::vector<int>> v((std::vector<std::vector<int>>)(r_list));
//return wrap(v[0][0] + 1);
'
settings=getPlugin("Rcpp")
settings$env$PKG_CXXFLAGS=paste('-std=c++11 ',settings$env$PKG_CXXFLAGS)
test_list = cxxfunction(signature(r_list="list"),body=code,plugin="Rcpp",settings=settings)

# l = list(a=1:10, b=c(9,8))
# 
# print(test_list(l))

##########################################################################################
# you can use a const reference to element of List
# it's of no use if you make a reference to elements of a list,
# because c++ have to transform the data structure, 
# so a copy is alway made
# Note that you cannot just make a reference to elements of list,
# you can make a const reference to elements of list, this is misleading ...
# I think it is good idea to transfor list to other data structures in C++ if you actually know the structure 
# of the list

code = 'List l(r_list);
std::vector<int> x = l[0];
x[0] += 1000;
return l;
'
settings=getPlugin("Rcpp")
settings$env$PKG_CXXFLAGS=paste('-std=c++0x ',settings$env$PKG_CXXFLAGS)
test_list = cxxfunction(signature(r_list="list"),body=code,plugin="Rcpp",settings=settings)

 l = list(a=1:10, b=c(9,8))

 print(test_list(l))

################################################################################################
code = 'List l(r_list);
std::set<int> x = as<std::set<int> >(l[0]);
//for(int i)
'
includes = "#include <vector>
#include <set>"
test_list = cxxfunction(signature(r_list="list"), body=code, includes=includes, plugin="Rcpp")
l = list(a=1:10, b=c(9,8))
print(test_list(l))
#########################################################################################
# cannot convert vector to set
code = '
std::set<int> x = as<std::set<int> >(r_vec);
//for(int i)
'
includes = "#include <vector>
#include <set>"
test_list = cxxfunction(signature(r_vec="vector"), body=code, includes=includes, plugin="Rcpp")
v = 1:10
test_list(v)

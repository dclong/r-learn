library(Rcpp)
library(inline)
#########################################################################################
# the containsElementNamed method accept const char * not std::string
code = 'List l(r_list);
const char * a = "a";
if(l.containsElementNamed(a)){
l["a"] = "wonderful!";
}
return l;
'
test_list = cxxfunction(signature(r_list="list"),body=code,plugin="Rcpp")

l = list(a=1:10, b='hahaha')

print(test_list(l))
###########################################################################################
# if a name does not exist, you cannot use it
# 
code = 'List l(r_list);
bool x = l.containsElementNamed("a");
return wrap(x);
'

printlist = cxxfunction(signature(r_list="list"), body=code, plugin='Rcpp')

printlist(alist)
#########################################################################################
#' not easy to work with names of vector
code = 'List l(r_list);
Vector<19>::NamesProxy x(l.names());
std::vector<std::string> y(as<std::vector<std::string>>(x));
return wrap(y);
'
list_name = cxxfunction(signature(r_list="list"),body=code,plugin="Rcpp")
l = list(a="a",b="bb")
print(list_name(l))

################################################################################################
#-----------------------------------------------------------------------------------
code = '
List l(r_list);
auto names = l.names();
std::cout << typeid(names).name() << std::endl;
'
includes = "
#include <iostream>
#include <typeinfo>
"
settings=getPlugin("Rcpp")
settings$env$PKG_CXXFLAGS=paste('-std=c++0x ',settings$env$PKG_CXXFLAGS)
list_name = cxxfunction(signature(r_list="list"),body=code, includes=includes, settings=settings)
l = list(a=1,b=2)
list_name(l)

src <- '
     Rcpp::NumericMatrix Am(A);
     int nrows = Am.nrow();
     int ncolumns = Am.ncol();
     for (int i = 0; i < ncolumns; i++) {
         for (int j = 1; j < nrows; j++) {
             Am(j,i) = Am(j,i) + Am(j-1,i);
         }
     }
     return Am;
 '
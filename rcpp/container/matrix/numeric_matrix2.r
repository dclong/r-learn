require(inline)
code <- '
    NumericMatrix m1 = Rcpp::as<NumericMatrix>(r_m1);
    NumericMatrix m2 = Rcpp::as<NumericMatrix>(r_m2);
    NumericMatrix m3 = m1 + m2;
    return wrap(m3);
 '
## create the compiled function
f <- cxxfunction(signature(r_m1="numeric",r_m2="numeric"), 
                       body=code, plugin="Rcpp")

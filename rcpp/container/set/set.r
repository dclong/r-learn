#-----------------------------------------------------------------------------------
#' this does not work
#' actually it is not a good idea to convert vector to set,
#' because set is sorted while vector is not necessarily sorted.
#' and a set contains distinct values while a vector not necessarily contains distinct values
library(Rcpp)
library(inline)
code='using namespace std;
set<string> x(as<set<string> >(r_vec));
return wrap(x.size());
'
test_vec = cxxfunction(signature(r_vec="vector"),body=code,plugin="Rcpp")
vec = as.character(1:10)
print(test_vec(vec))

#-----------------------------------------------------------------------------------
#' a possible way is to get names of list in R and then pass the vector of names 
#' to c++ as a vector of strings
library(Rcpp)
library(inline)
#-------------------------------------------------------------------------------
code='
using namespace std;
vector<string> x(as<vector<string> >(r_vec));
return wrap(x.size());
'
test_vec = cxxfunction(signature(r_vec="vector"),body=code,plugin="Rcpp")
vec = as.character(1:10)
print(test_vec(vec))
#----------------------------------------------------------------------------------
code='
using namespace std;
NumericVector x(as<NumericVector>(r_vec));
size_t size = x.size();
std::cout << typeid(size).name() << std::endl;
'
includes="
#include <typeinfo>
#include <iostream>
"
settings=getPlugin("Rcpp")
settings$env$PKG_CXXFLAGS=paste('-std=c++11 ',settings$env$PKG_CXXFLAGS)
test_vec = cxxfunction(signature(r_vec="vector"),body=code,includes=includes,settings=settings)
vec = 1:10
print(test_vec(vec))
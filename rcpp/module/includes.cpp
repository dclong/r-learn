#include <unordered_set>
#include <unordered_map>
#include <random>
#include <iterator>
#include <tuple>
//using namespace Rcpp;
//using namespace std;
double sq_eu_dist(double x1, double y1, double x2, double y2){
    double xdiff = x1 - x2;
    double ydiff = y1 - y2;
    return xdiff * xdiff + ydiff * ydiff;
}

//double sq_eu_dist(const std::vector<double> & xs, const std::vector<double> & ys){
    
//}

std::unordered_set<int> neighbors1(int index, const arma::mat & dist, double epsilon){
    int size = dist.n_rows;
    std::unordered_set<int> neis;
    for(int i=0; i<size; ++i){
        if(i!=index && dist(index,i)<epsilon){
            neis.insert(i);
        }
    }
    return neis;
}

std::vector<std::unordered_set<int>> neighbors(const arma::mat & dist, double epsilon){
    int size = dist.n_rows;
    std::vector<std::unordered_set<int>> neis(size);
    for(int i=0; i<size; ++i){
        neis[i] = neighbors1(i, dist, epsilon);
    }
    return neis;
}


bool is_boundry(int index, const std::vector<int> & gs, const arma::mat & dist, double epsilon, double lower_ratio){
    int c0 = gs[index];
    std::unordered_set<int> neis(neighbors1(index,dist,epsilon));
    int n_total(neis.size()+1);
    int n_others(0);
    for(std::unordered_set<int>::iterator it(neis.begin()); it!=neis.end(); ++it){
        if(gs[*it]!=c0){
            ++ n_others;
        }
    }
    double other_ratio(n_others / (double) n_total);
    return other_ratio > lower_ratio;
}

std::unordered_set<int> boundry(const std::vector<int> & gs, const arma::mat & dist, double epsilon, double lower_ratio){
    std::unordered_set<int> b;
    int size = gs.size();
    for(int i=0; i<size; ++i){
        if(is_boundry(i,gs,dist,epsilon,lower_ratio)){
            b.insert(i+1);
        }
    }
    return b;
}

int smooth_class(int index, const std::vector<std::unordered_set<int>> & neis, 
const std::vector<int> & gs, double min_ratio){
    const std::unordered_set<int> & neis_index(neis[index]);
    int total = neis_index.size();
    std::map<int,int> n;
    for(int i : neis_index){
        ++ n[gs[i]];
    }
    if(n[gs[index]]/(double)total<min_ratio){
        return std::max_element(n.begin(),n.end(),
        [&n](const std::pair<int,int> & x1, const std::pair<int,int> & x2){
            return x1.second < x2.second;
        }) -> first;
    }
    return gs[index];
}

int smooth1(int index, const arma::mat & dist, 
const std::vector<int> & gs, double epsilon, double min_ratio){
    return smooth_class(index, neighbors(dist,epsilon), gs, min_ratio);
}

arma::Mat<int> smooth(const arma::mat & dist, 
const std::vector<int> & gs, double epsilon, double min_ratio,bool recur){
    std::vector<std::unordered_set<int>> neis {neighbors(dist,epsilon)};
    int size = gs.size();
    std::vector<int> smooth_gs(gs);
    bool updated(false);
    for(int i=0; i<size; ++i){
        int c = smooth_class(i, neis, gs, min_ratio);
        smooth_gs[i] = c;
        if(c!=gs[i]){
            updated = true;
        }
    }
    if(updated&&recur){
        return arma::join_rows(arma::Mat<int>(smooth_gs),smooth(dist,smooth_gs,epsilon,min_ratio,recur));
    }
    return arma::Mat<int>(smooth_gs);
}

std::unordered_map<int, std::unordered_set<int>> partition_i(const std::vector<int> & gs){
    std::unordered_map<int, std::unordered_set<int>> p;
    int size(gs.size());
    for(int i(0); i<size; ++i){
        p[gs[i]].insert(i);
    }
	return p;
}

/**
* Check if two groups are neighbors.
**/
bool is_group_neighbor(const std::unordered_set<int> & g1, const std::unordered_set<int> & g2,  
const arma::mat & dist, double epsilon, int k){
    int n(0);
    for(std::unordered_set<int>::const_iterator i=g1.begin(); i!=g1.end(); ++i){
        for(std::unordered_set<int>::const_iterator j(g2.begin()); j!=g2.end(); ++j){
            if(dist(*i,*j)<epsilon){
                ++ n;
            }
        }
    }
    return n >= k;
}

/**
* Find neighbors of a group.
**/
std::unordered_set<int> group_neighbor1(const int gi, 
const std::unordered_map<int, std::unordered_set<int>> & gpar, 
const arma::mat & dist, double epsilon, double k){
    std::unordered_set<int> gneis;
    const std::unordered_set<int> & gm(gpar.at(gi));
    for(std::unordered_map<int,std::unordered_set<int>>::const_iterator it(gpar.begin()); it!=gpar.end(); ++it){
        if(it->first!=gi&&is_group_neighbor(gm, it->second, dist, epsilon, k)){
            gneis.insert(it->first);
        }
    }
    return gneis;
}
/**
* Find neighbors for every group.
**/
std::unordered_map<int, std::unordered_set<int>> 
group_neighbors(const std::unordered_map<int, std::unordered_set<int>> & gpar, 
const arma::mat & dist, double epsilon, double k){
    std::unordered_map<int, std::unordered_set<int>> gneis;
    for(std::unordered_map<int, std::unordered_set<int>>::const_iterator it(gpar.begin()); it!=gpar.end(); ++it){
        gneis[it->first] = group_neighbor1(it->first, gpar, dist, epsilon, k);
    }
    return gneis;
}
/**
 * Average distance of an index to all elements in a set.
 * */
std::tuple<double, double, int> minave_distance(const int index, const std::set<int> & s){
    std::vector<int> ds(s.size());
    std::transform(s.begin(),s.end(),ds.begin(),[index](const int x){
        return std::abs(x-index);
    });
    double ave = std::accumulate(ds.begin(),ds.end(),0.0) / ds.size();
    double min = *std::min_element(ds.begin(),ds.end());
    return std::make_tuple(min, ave, index);
}
/**
 * All distances (as a vector) of an index to all elements in a set.
 * 
std::vector<int> index_distances(const int index, const std::set<int> & s){
    int size = s.size();
    std::vector<int> ds(size);
    for(int i=0,std::set<int>::const_iterator it(s.begin()); i<size; ++i,++it){
        ds[i] = std::abs(*it - index);
    }
    return ds;
}*/
/**
 * Return an index that is the furthest from current index on average.
 * */
int furthest_index(const std::set<int> & used_colors, const std::set<int> & av_colors){
    if(av_colors.size()==0){
        return 0;
    }
    if(used_colors.size()==0){
        return 1;
    }
    std::vector<std::tuple<double,double,int>> ds;
    for(std::set<int>::const_iterator it(av_colors.begin()); it!=av_colors.end(); ++it){
        ds.push_back(minave_distance(*it,used_colors));
    }
    return std::get<2>(*std::max_element(ds.begin(),ds.end()));
}
/**
* Find available colors for a group.
**/
int select_index(const std::unordered_set<int> & neis, 
const std::unordered_map<int,int> & gcolor, int max_index){
    const static std::set<int> all_indexes{[max_index](){
        std::set<int> x;
        for(int i=1; i<=max_index; ++i){
            x.insert(i);
        }
        return x;
    }()};
    std::set<int> used_indexes;
    for(std::unordered_set<int>::const_iterator it(neis.begin()); it!=neis.end(); ++it){
        if(gcolor.count(*it)>0 && gcolor.at(*it)>0){
            used_indexes.insert(gcolor.at(*it));
        }
    }
    std::set<int> av_indexes;
    std::set_difference(all_indexes.begin(), all_indexes.end(), 
        used_indexes.begin(), used_indexes.end(), 
            std::inserter(av_indexes, av_indexes.end()));
    return furthest_index(used_indexes, av_indexes);
}
/**
* Randomly select a color.
*
int random_index(const std::set<int> & av_colors, std::mt19937 & rng){
    int size = av_colors.size();
    if(size>0){
        std::uniform_int_distribution<int> uidist(0, size-1);
        return *next(av_colors.begin(), uidist(rng));
    }
    return 0;
}*/

/**
* Automatically select color for different groups. 
* The color index is from 1 to 8.
**/
std::vector<int> differentiate_indexes(const std::vector<int> & gs, 
const arma::mat & dist, int max_index, double epsilon, double k){
    std::unordered_map<int, std::unordered_set<int>> gneis(group_neighbors(partition_i(gs),dist,epsilon,k));
    std::unordered_map<int, int> gindex;
    for(std::unordered_map<int, std::unordered_set<int>>::iterator it(gneis.begin()); it!=gneis.end(); ++it){
        gindex[it->first] = select_index(it->second, gindex, max_index);
    }
    int size = gs.size();
    std::vector<int> color(size);
    for(int i=0; i<size; ++i){
        color[i] = gindex[gs[i]];
    }
    return color;
}

RCPP_MODULE(mod){
    function("sq_eu_dist",&sq_eu_dist);
    //function("smooth_class",&smooth_class);
    function("smooth1",&smooth1);
    function("smooth",&smooth);
    function("neighbors1", &neighbors1);
    function("neighbors", &neighbors);
    function("is_boundry",&is_boundry);
    function("boundry",&boundry);
    function("differentiate_indexes",&differentiate_indexes);
}


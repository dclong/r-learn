require(inline)
src <- '
using namespace Rcpp;
arma::rowvec r(as<arma::rowvec>(r_vec));
arma::mat m(10000, r.size());
for(int i=0; i<m.n_rows; ++i){
    shuffle_left(r.begin(), r.end(), r.size());
    m.row(i) = r;
}
return(wrap(m));
'
includes = '
template<typename InputIt> void shuffle_left(InputIt first, InputIt last, int k) {
RNGScope scope;
int n = last - first;
int n_minus_one = n - 1;
int upperBound = k;
//no need to shuffle if only 1 element is left
if (upperBound>=n) {
upperBound = n_minus_one;
}
for (int i=0; i<upperBound; ++i) {
//generate an index between i and n - 1 (inclusive)
int index = static_cast<int>(Rf_runif(i, n));
index = std::min(index, n);
index = std::max(index, i-1);
// size_t index = k;
//swap x[i] and x[index]
std::swap(*(first + i), *(first + index));
}
}
'
mat = cxxfunction(signature(r_vec="Numeric"),body=src, plugin="RcppArmadillo", includes=includes)

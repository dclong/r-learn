require(inline)
src <- '
using namespace Rcpp;
double r = Rf_runif(10,11);
return(wrap(r));
'

rand = cxxfunction(signature(r_m="Numeric"),body=src, plugin="RcppArmadillo")
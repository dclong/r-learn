library(inline)
library(Rcpp)

src = "
using namespace Rcpp;
arma::mat Y(as<arma::mat>(r_Y));
arma::mat X(as<arma::mat>(r_X));
return wrap(spt(Y, max_abs_corr0, max_abs_corr, 10, 1000, X));
"
includes = paste(readText(file='/home/dclong/Dropbox/code/r/package/spt2/dclong.spt/inst/test_statistics.cpp'), "\n\n", 
                 readText(file='/home/dclong/Dropbox/code/r/package/spt2/dclong.spt/inst/spt.cpp'),sep="")

settings=getPlugin("RcppArmadillo")
settings$env$PKG_CXXFLAGS=paste('-std=c++0x ',settings$env$PKG_CXXFLAGS)
fx = cxxfunction(signature(r_Y="numeric", r_X="numeric"),body=src,includes=includes, 
                 plugin="RcppArmadillo",settings=settings)

system.time(fx(Y,X)->fx.out)->j


X = scale(t(marker))
# center and standardized rows of barley
Y = t(scale(t(barley)))
#include <iostream>
int f(std::vector<int> x){
    return x[10];
}

int g(arma::Col<int> x){
    try{
	    return x(10);
	} catch(std::logic_error e){
		std::cout << e.what() << std::endl;
	}
    return x(0);
}

RCPP_MODULE(mod){
    function("f",&f);
    function("g",&g);
}

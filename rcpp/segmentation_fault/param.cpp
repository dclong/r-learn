
double f(double x1, double x2){
  return x1 + x2;
}


RCPP_MODULE(mod){
  function("f",&f);
}
library(dclong.fs)
library(inline)
library(Rcpp)
includes = readText('segmentation_fault/bound.cpp')

settings=getPlugin("RcppArmadillo")
settings$env$PKG_CXXFLAGS=paste('-std=c++0x ',settings$env$PKG_CXXFLAGS)
fx = cxxfunction(signature(),body="",includes=includes, 
                 plugin="RcppArmadillo",settings=settings)

mod = Module( "mod" , getDynLib(fx) , mustStart=T)
print(mod)

library(Rcpp)
library(inline)
src = '
int n = as<int>(r_n);
int index = as<int>(r_index);
int nsim = as<int>(r_nsim);
RNGScope scope;
std::vector<int> x(n);
for(int i=0; i<n; ++i){
    x[i] = i;
}
std::vector<int> freq(n);
for(int i = 0; i < nsim; ++i){
    shuffle(x.begin(), x.end());
    ++freq[x[index]];
}
return wrap(freq);
'
includes = "
#include <algorithm>
template<typename InputIt> void shuffle(InputIt first, InputIt last){
    long int i = std::distance(first, last);
    while(i > 1){
        //generate an integer between 0 and i - 1 (both inclusive)
        long int index = Rf_runif(0, i);
        --i;
        index = std::min(std::max(0L, index), i); // just to be safe, probably not needed
        //swap x[i] and x[index]
        std::swap(*std::next(first, i), *std::next(first, index));
    }
}"
settings=getPlugin("Rcpp")
settings$env$PKG_CXXFLAGS=paste('-std=c++11 -I /home/dclong/archives/code/cpp/hmm/',settings$env$PKG_CXXFLAGS)
shuffle = cxxfunction(signature(r_n="Integer", r_index="Integer", r_nsim="Integer"), 
                         body=src, includes=includes, settings=settings)




code = '
Function f(fun);
SEXP x = f();
int y = as<int>(x);
return wrap(y);
'

rwrap = cxxfunction(signature(fun="function"),code,plugin="Rcpp")
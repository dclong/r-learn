library(Rcpp)
library(inline)
code = '
std::vector<double> x(as<std::vector<double> >(r_vec));
for(std::vector<double>::iterator it(x.begin()); it!=x.end(); ++it){
    std::cout << *it << " ";
}
std::cout << std::endl;
return wrap(x.size());
'
includes = "
#include <iostream>
#include <vector>
"
f = cxxfunction(signature(r_vec="numeric"), includes=includes, body=code, plugin="Rcpp")





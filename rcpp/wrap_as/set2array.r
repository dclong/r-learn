# you can wrap a set as an array
code = '
std::set<int> x {1,5,2,4,9};
return wrap(x);
'
includes = "
#include <set>
"
settings=getPlugin("Rcpp")
settings$env$PKG_CXXFLAGS=paste('-std=c++0x ',settings$env$PKG_CXXFLAGS)
test_set = cxxfunction(body=code, includes=includes, plugin="Rcpp",settings=settings)
test_set()

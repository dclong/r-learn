
read.csv(file="moodys/bmo_all_st_clean.csv", as.is=TRUE) -> st_moody
read.csv(file="ben/bmo_all_st_clean.csv", as.is=TRUE) -> st_ben
colnames(st_moody) -> cns_st_moody
colnames(st_ben) -> cns_st_ben
# I've verified that the 2 datasets have the same variables even though column/variable names might be different
colnames(st_moody) = cns_st_ben
colnames(st_ben) = cns_st_ben
# Moody's bmo_all_st_clean has 10 (108 actually but 89 of them are non-sense records) more rows than mine
nrow(st_moody) - nrow(st_ben)
# From the meaning of static, loanID should be unique for both tables, 
# but it is not the case for st_moody. 
# This makes me trust my data more!!!
length(st_moody$loanID) - length(unique(st_moody$loanID))
length(st_ben$loanID) - length(unique(st_ben$loanID))
# It turns out to be that st_moody$loanID contains "IN" and "US" which does not make sense
# Those are the loanID that cannot be converted to numbers!!!
# The records with loanID being "IN" or "US" will be removed
st_moody[st_moody$loanID != "IN" & st_moody$loanID != "US", ] -> st_moody 
# convert the $st_moody$loanID to numeric
st_moody$loanID = as.numeric(st_moody$loanID)
# Moody's bmo_all_st_clean has 10 more rows than mine
nrow(st_moody) - nrow(st_ben)
# use loanID_time as row names
paste(st_moody$loanID, st_moody$yyyy_mm, sep="_") -> rownames(st_moody)
paste(st_ben$loanID, st_ben$yyyy_mm, sep="_") -> rownames(st_ben)
# I've verified that all rows in st_ben are in st_moody
all(rownames(st_ben) %in% rownames(st_moody))
# keep only the records in st_ben
st_moody = st_moody[rownames(st_ben), ]

# loanID: all match
all(st_moody$loanID == st_ben$loandID)
# lienPosition: all match
all(st_moody$lienPosition == st_ben$lienPosition)
# c_interest_only: all match 
all(st_moody$c_interest_only == st_ben$c_interest_only)
all(st_moody$c_interest_only == st_ben$c_interest_only, na.rm=T)
all(is.na(st_moody$c_interest_only) == is.na(st_ben$c_interest_only))
# Property: all match 
all(st_moody$Property == st_ben$Property)
# Occupancy: all match 
all(st_moody$Occupancy == st_ben$Occupancy)
all(st_moody$Occupancy == st_ben$Occupancy, na.rm=T)
all(is.na(st_moody$Occupancy) == is.na(st_ben$Occupancy))
# Purpose: all match 
all(st_moody$Purpose == st_ben$Purpose)
all(st_moody$Purpose == st_ben$Purpose, na.rm=T)
all(is.na(st_moody$Purpose) == is.na(st_ben$Purpose))
# RateType: all match 
all(st_moody$RateType == st_ben$RateType)
# FixedRatePeriod: all match 
all(st_moody$FixedRatePeriod == st_ben$FixedRatePeriod)
all(st_moody$FixedRatePeriod == st_ben$FixedRatePeriod, na.rm=T)
all(is.na(st_moody$FixedRatePeriod) == is.na(st_ben$FixedRatePeriod))
# Documentation: all match 
all(st_moody$Documentation == st_ben$Documentation)
# IOTerm: all match
all(st_moody$IOTerm == st_ben$IOTerm)
all(st_moody$IOTerm == st_ben$IOTerm, na.rm=T)
all(is.na(st_moody$IOTerm) == is.na(st_ben$IOTerm))
max(abs(st_moody$IOTerm-st_ben$IOTerm), na.rm=T)
# PrepayPenaltyTerm: some big differences, need to come back later 
all(as.numeric(st_moody$PrepayPenaltyTerm) == st_ben$PrepayPenaltyTerm)
all(as.numeric(st_moody$PrepayPenaltyTerm) == st_ben$PrepayPenaltyTerm, na.rm=T)
all(is.na(as.numeric(st_moody$PrepayPenaltyTerm)) == is.na(st_ben$PrepayPenaltyTerm))
# DTI: almost all the same: non-NAs match, NA: 0.9999825
all(as.numeric(st_moody$DTI) == st_ben$DTI)
all(as.numeric(st_moody$DTI) == st_ben$DTI, na.rm=T)
all(is.na(as.numeric(st_moody$DTI)) == is.na(st_ben$DTI))
mean(is.na(as.numeric(st_moody$DTI)) == is.na(st_ben$DTI))
max(abs(as.numeric(st_moody$DTI)-st_ben$DTI), na.rm=T)
# IsModified: all match
all(as.numeric(st_moody$IsModified) == st_ben$IsModified)
all(as.numeric(st_moody$IsModified) == st_ben$IsModified, na.rm=T)
all(is.na(as.numeric(st_moody$IsModified)) == is.na(st_ben$IsModified))
# IsBroker: all match
all(as.numeric(st_moody$IsBroker) == st_ben$IsBroker)
all(as.numeric(st_moody$IsBroker) == st_ben$IsBroker, na.rm=T)
all(is.na(as.numeric(st_moody$IsBroker)) == is.na(st_ben$IsBroker))
# IsHeloc: almost all the same: non-NAs match, NA: 0.9997774
all(as.numeric(st_moody$IsHeloc) == st_ben$IsHeloc)
all(as.numeric(st_moody$IsHeloc) == st_ben$IsHeloc, na.rm=T)
all(is.na(as.numeric(st_moody$IsHeloc)) == is.na(st_ben$IsHeloc))
mean(is.na(as.numeric(st_moody$IsHeloc)) == is.na(st_ben$IsHeloc))
max(abs(as.numeric(st_moody$IsHeloc)-st_ben$IsHeloc), na.rm=T)

# ump_region: 0.9973243
all(st_moody$ump_region == st_ben$ump_region)
mean(st_moody$ump_region == st_ben$ump_region)
all(st_moody$ump_region == st_ben$ump_region, na.rm=T)
all(is.na(st_moody$ump_region) == is.na(st_ben$ump_region))
mean(is.na(st_moody$ump_region) == is.na(st_ben$ump_region))

# cntrct_cd: 0.9995092 
all(as.numeric(st_moody$cntrct_cd) == st_ben$cntrct_cd)
mean(as.numeric(st_moody$cntrct_cd) == st_ben$cntrct_cd)
mean(as.numeric(st_moody$cntrct_cd) == st_ben$cntrct_cd, na.rm=TRUE)
mean(is.na(as.numeric(st_moody$cntrct_cd)) == is.na(st_ben$cntrct_cd))
all(as.numeric(st_moody$cntrct_cd) == st_ben$cntrct_cd, na.rm=T)
all(is.na(as.numeric(st_moody$cntrct_cd)) == is.na(st_ben$cntrct_cd))
mean(is.na(as.numeric(st_moody$cntrct_cd)) == is.na(st_ben$cntrct_cd))
max(abs(as.numeric(st_moody$cntrct_cd)-st_ben$cntrct_cd), na.rm=T)

# IsHarris: 0.9999825
all(as.numeric(st_moody$IsHarris) == st_ben$IsHarris)
mean(as.numeric(st_moody$IsHarris) == st_ben$IsHarris)
mean(as.numeric(st_moody$IsHarris) == st_ben$IsHarris, na.rm=TRUE)
mean(is.na(as.numeric(st_moody$IsHarris)) == is.na(st_ben$IsHarris))
all(as.numeric(st_moody$IsHarris) == st_ben$IsHarris, na.rm=T)
all(is.na(as.numeric(st_moody$IsHarris)) == is.na(st_ben$IsHarris))
mean(is.na(as.numeric(st_moody$IsHarris)) == is.na(st_ben$IsHarris))
max(abs(as.numeric(st_moody$IsHarris)-st_ben$IsHarris), na.rm=T)

# DrawdownTerm: 0.9999699
all(as.numeric(st_moody$DrawdownTerm) == st_ben$DrawdownTerm)
mean(as.numeric(st_moody$DrawdownTerm) == st_ben$DrawdownTerm)
mean(as.numeric(st_moody$DrawdownTerm) == st_ben$DrawdownTerm, na.rm=TRUE)
mean(is.na(as.numeric(st_moody$DrawdownTerm)) == is.na(st_ben$DrawdownTerm))
all(as.numeric(st_moody$DrawdownTerm) == st_ben$DrawdownTerm, na.rm=T)
all(is.na(as.numeric(st_moody$DrawdownTerm)) == is.na(st_ben$DrawdownTerm))
max(abs(as.numeric(st_moody$DrawdownTerm)-st_ben$DrawdownTerm), na.rm=T)

# MaxDrawnAmount: 0.9990026
all(as.numeric(st_moody$MaxDrawnAmount) == st_ben$MaxDrawnAmount)
mean(as.numeric(st_moody$MaxDrawnAmount) == st_ben$MaxDrawnAmount)
mean(as.numeric(st_moody$MaxDrawnAmount) == st_ben$MaxDrawnAmount, na.rm=TRUE)
mean(is.na(as.numeric(st_moody$MaxDrawnAmount)) == is.na(st_ben$MaxDrawnAmount))
all(as.numeric(st_moody$MaxDrawnAmount) == st_ben$MaxDrawnAmount, na.rm=T)
all(is.na(as.numeric(st_moody$MaxDrawnAmount)) == is.na(st_ben$MaxDrawnAmount))
max(abs(as.numeric(st_moody$MaxDrawnAmount)-st_ben$MaxDrawnAmount), na.rm=T)

# ORIG_INT_RT: 0.9997714
all(as.numeric(st_moody$ORIG_INT_RT) == st_ben$ORIG_INT_RT)
mean(as.numeric(st_moody$ORIG_INT_RT) == st_ben$ORIG_INT_RT)
mean(as.numeric(st_moody$ORIG_INT_RT) == st_ben$ORIG_INT_RT, na.rm=TRUE)
mean(is.na(as.numeric(st_moody$ORIG_INT_RT)) == is.na(st_ben$ORIG_INT_RT))
all(as.numeric(st_moody$ORIG_INT_RT) == st_ben$ORIG_INT_RT, na.rm=T)
all(is.na(as.numeric(st_moody$ORIG_INT_RT)) == is.na(st_ben$ORIG_INT_RT))
max(abs(as.numeric(st_moody$ORIG_INT_RT)-st_ben$ORIG_INT_RT), na.rm=T)

# FICO: 0.9997849
all(as.numeric(st_moody$FICO) == st_ben$FICO)
mean(as.numeric(st_moody$FICO) == st_ben$FICO)
mean(as.numeric(st_moody$FICO) == st_ben$FICO, na.rm=TRUE)
mean(is.na(as.numeric(st_moody$FICO)) == is.na(st_ben$FICO))
all(as.numeric(st_moody$FICO) == st_ben$FICO, na.rm=T)
all(is.na(as.numeric(st_moody$FICO)) == is.na(st_ben$FICO))
max(abs(as.numeric(st_moody$FICO)-st_ben$FICO), na.rm=T)

# Zip: 0.9997774
all(st_moody$Zip == st_ben$Zip)
mean(st_moody$Zip == st_ben$Zip)
mean(as.numeric(st_moody$Zip) == st_ben$Zip, na.rm=TRUE)
mean(is.na(as.numeric(st_moody$Zip)) == is.na(st_ben$Zip))
all(st_moody$Zip == st_ben$Zip, na.rm=T)
all(is.na(st_moody$Zip) == is.na(st_ben$Zip))

# State: 0.9971392
all(st_moody$State == st_ben$State)
mean(st_moody$State == st_ben$State)
mean(as.numeric(st_moody$State) == st_ben$State, na.rm=TRUE)
mean(is.na(as.numeric(st_moody$State)) == is.na(st_ben$State))
all(st_moody$State == st_ben$State, na.rm=T)
all(is.na(st_moody$State) == is.na(st_ben$State))

# AmorTerm: 0.9999828
all(st_moody$AmorTerm == st_ben$AmorTerm)
mean(st_moody$AmorTerm == st_ben$AmorTerm)
mean(as.numeric(st_moody$AmorTerm) == st_ben$AmorTerm, na.rm=TRUE)
mean(is.na(as.numeric(st_moody$AmorTerm)) == is.na(st_ben$AmorTerm))
max(abs(st_moody$AmorTerm-st_ben$AmorTerm), na.rm=T)

# OriginalTerm: 0.9998449
all(st_moody$OriginalTerm == st_ben$OriginalTerm)
mean(st_moody$OriginalTerm == st_ben$OriginalTerm)
mean(as.numeric(st_moody$OriginalTerm) == st_ben$OriginalTerm, na.rm=TRUE)
mean(is.na(as.numeric(st_moody$OriginalTerm)) == is.na(st_ben$OriginalTerm))
max(abs(st_moody$OriginalTerm-st_ben$OriginalTerm), na.rm=T)

# Srbalance: 0.9991931
all(st_moody$Srbalance == st_ben$Srbalance)
mean(st_moody$Srbalance == st_ben$Srbalance)
mean(as.numeric(st_moody$Srbalance) == st_ben$Srbalance, na.rm=TRUE)
mean(is.na(as.numeric(st_moody$Srbalance)) == is.na(st_ben$Srbalance))
max(abs(st_moody$Srbalance-st_ben$Srbalance), na.rm=T)

# OriginalAmount: 0.9999519
all(st_moody$OriginalAmount == st_ben$OriginalAmount)
mean(st_moody$OriginalAmount == st_ben$OriginalAmount)
mean(as.numeric(st_moody$OriginalAmount) == st_ben$OriginalAmount, na.rm=TRUE)
mean(is.na(as.numeric(st_moody$OriginalAmount)) == is.na(st_ben$OriginalAmount))
max(abs(st_moody$OriginalAmount-st_ben$OriginalAmount), na.rm=T)

# JrLTV: 0.9981972
all(st_moody$JrLTV == st_ben$JrLTV)
mean(st_moody$JrLTV == st_ben$JrLTV)
mean(as.numeric(st_moody$JrLTV) == st_ben$JrLTV, na.rm=TRUE)
mean(as.numeric(st_moody$JrLTV)-st_ben$JrLTV<1e-5, na.rm=TRUE)
mean(is.na(as.numeric(st_moody$JrLTV)) == is.na(st_ben$JrLTV))
max(abs(st_moody$JrLTV-st_ben$JrLTV), na.rm=T)

# LTV: 0.9976991
all(st_moody$LTV == st_ben$LTV)
mean(st_moody$LTV == st_ben$LTV)
mean(as.numeric(st_moody$LTV) == st_ben$LTV, na.rm=TRUE)
mean(as.numeric(st_moody$LTV)-st_ben$LTV<1e-5, na.rm=TRUE)
mean(is.na(as.numeric(st_moody$LTV)) == is.na(st_ben$LTV))
max(abs(st_moody$LTV-st_ben$LTV), na.rm=T)
# CombinedLTV: 0.9916766 
all(st_moody$CombinedLTV == st_ben$CombinedLTV)
mean(st_moody$CombinedLTV == st_ben$CombinedLTV)
mean(as.numeric(st_moody$CombinedLTV) == st_ben$CombinedLTV, na.rm=TRUE)
mean(as.numeric(st_moody$CombinedLTV)-st_ben$CombinedLTV<1e-5, na.rm=TRUE)
mean(is.na(as.numeric(st_moody$CombinedLTV)) == is.na(st_ben$CombinedLTV))
max(abs(st_moody$CombinedLTV-st_ben$CombinedLTV), na.rm=T)
# OriginalAppraisalAmount: almost identical
all(st_moody$OriginalAppraisalAmount == st_ben$OriginalAppraisalAmount)
mean(st_moody$OriginalAppraisalAmount == st_ben$OriginalAppraisalAmount)
mean(as.numeric(st_moody$OriginalAppraisalAmount) == st_ben$OriginalAppraisalAmount, na.rm=TRUE)
mean(as.numeric(st_moody$OriginalAppraisalAmount)-st_ben$OriginalAppraisalAmount<1e-5, na.rm=TRUE)
mean((as.numeric(st_moody$OriginalAppraisalAmount)-st_ben$OriginalAppraisalAmount)/as.numeric(st_moody$OriginalAppraisalAmount)<1e-7, na.rm=TRUE)
mean(is.na(as.numeric(st_moody$OriginalAppraisalAmount)) == is.na(st_ben$OriginalAppraisalAmount))
max(abs(st_moody$OriginalAppraisalAmount-st_ben$OriginalAppraisalAmount), na.rm=T)
#-------------------------------------------------------------------------------------
# OriginationDate: difference due to format, if the format is corrected, will probably match
all(as.numeric(st_moody$OriginationDate) == st_ben$OriginationDate)
mean(as.numeric(st_moody$OriginationDate) == st_ben$OriginationDate)
mean(as.numeric(st_moody$OriginationDate) == st_ben$OriginationDate, na.rm=TRUE)
mean(is.na(as.numeric(st_moody$OriginationDate)) == is.na(st_ben$OriginationDate))
all(as.numeric(st_moody$OriginationDate) == st_ben$OriginationDate, na.rm=T)
all(is.na(as.numeric(st_moody$OriginationDate)) == is.na(st_ben$OriginationDate))
max(abs(as.numeric(st_moody$OriginationDate)-st_ben$OriginationDate), na.rm=T)
#-------------------------------------------------------------------------------------
# hpi_region: some big differences, need to come back later 
all(st_moody$hpi_region == st_ben$hpi_region)
mean(st_moody$hpi_region == st_ben$hpi_region)
mean(st_moody$hpi_region == st_ben$hpi_region)
all(st_moody$hpi_region == st_ben$hpi_region, na.rm=T)
all(is.na(st_moody$hpi_region) == is.na(st_ben$hpi_region))
mean(is.na(st_moody$hpi_region) == is.na(st_ben$hpi_region))






unpivot = function(x){
    m = nrow(x)
    n = ncol(x)
    cns = colnames(x)
    rep(cns[-1], each=m) -> transition
    period_end_dt = rep(x[, 1], times=n-1)
    value = as.vector(as.matrix(x[, -1]))
    data.frame(period_end_dt=period_end_dt, transition=transition, value=value) -> pivoted
    pivoted
}
